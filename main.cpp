/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include <iostream>
#include "tests/UnitTestTeacher.h"
#include "arcenciel/Context.h"
#include "tests/UnitTestTiny.h"
#include "tests/IntegrationTest.h"
#include "arcenciel/Proba.h"

void tests_unitaires();
void choisir_context(int numero_context, Context &context, std::string &nom_contexte);
void afficher_contexts_disponibles() ;
void tester(int argc, char *const *argv);
void tests_integration(int argc, char *const *argv);
void probas(uint64_t taille_dictionnaire);
void probas_essais();

void probas_essais_94();

int main(int argc, char* argv[]) {

    if(argc == 1) {
        std::cout << "usage : " << std::endl;
        std::cout << "    ./main --unitaires                  : tests unitaires" << std::endl;
        std::cout << "    ./main --probas <tailleDico>        : probas" << std::endl;
        std::cout << "    ./main --creer <numContext>         : création de la table pour le contexte <numContext>" << std::endl;
        std::cout << "    ./main --creer-multi                : création de plusieurs tables (partie Moult Tables)" << std::endl;
        std::cout << "    ./main --crack-massive <numContext> : crackage de 1000 hashés pour le contexte <numContext>" << std::endl;
        std::cout << "    ./main --crack-example <numContext> : crackage de 10 hashés pour le contexte <numContext>" << std::endl;
        std::cout << "    ./main --crack <numContext> <hash>  : crackage de <hash> pour le contexte <numContext>" << std::endl;
        std::cout << "    ./main --crack-multi-example        : crackage de 10 hashés (partie Moult Tables)" << std::endl;
        std::cout << "    ./main --crack-multi <hash>         : crackage de <hash> (partie Moult Tables)" << std::endl;

        std::cout << std::endl << "Contextes disponibles : " << std::endl;

        afficher_contexts_disponibles();
    } else {
        tester(argc, argv);
    }

    return 0;
}

void tester(int argc, char *const *argv) {

    if(strcmp(argv[1], "--unitaires") == 0) {
        tests_unitaires();
    } else if(strcmp(argv[1], "--probas") == 0) {
        probas((uint64_t) atoi(argv[2]));
    } else if(strcmp(argv[1], "--probas-test") == 0) {
        probas_essais();
    } else if(strcmp(argv[1], "--probas-test-94") == 0) {
        probas_essais_94();
    } else {
        tests_integration(argc, argv);
    }
}

void probas_essais_94() {

    IntegrationTest integrationTest;

    Context context_conseille_1(5, "abcdefghijklmnopqrstuvwxyz0123456789", 777, 20000 * 12);
    context_conseille_1.afficher();
    integrationTest.test_multi_table(context_conseille_1);
    integrationTest.test_multi_crack_exemples(context_conseille_1);

    Context context_conseille_2(5, "abcdefghijklmnopqrstuvwxyz0123456789", 666, 20000 * 14);
    context_conseille_2.afficher();
    integrationTest.test_multi_table(context_conseille_2);
    integrationTest.test_multi_crack_exemples(context_conseille_2);

    Context context_conseille_3(5, "abcdefghijklmnopqrstuvwxyz0123456789", 6665, 20000 * 2);
    context_conseille_3.afficher();
    integrationTest.test_multi_table(context_conseille_3);
    integrationTest.test_multi_crack_exemples(context_conseille_3);

    Context context_conseille_4(5, "abcdefghijklmnopqrstuvwxyz0123456789", 555, 20000 * 17);
    context_conseille_4.afficher();
    integrationTest.test_multi_table(context_conseille_4);
    integrationTest.test_multi_crack_exemples(context_conseille_4);
}

void probas_essais() {

    IntegrationTest integrationTest;

    Context context_conseille_1(5, "abcdefghijklmnopqrstuvwxyz0123456789", 333, 20000 * 13);
    context_conseille_1.afficher();
    integrationTest.test_multi_table(context_conseille_1);
    integrationTest.test_multi_crack_exemples(context_conseille_1);

    Context context_conseille_2(5, "abcdefghijklmnopqrstuvwxyz0123456789", 555, 20000 * 8);
    context_conseille_2.afficher();
    integrationTest.test_multi_table(context_conseille_2);
    integrationTest.test_multi_crack_exemples(context_conseille_2);

    Context context_conseille_3(5, "abcdefghijklmnopqrstuvwxyz0123456789", 444, 20000 * 10);
    context_conseille_3.afficher();
    integrationTest.test_multi_table(context_conseille_3);
    integrationTest.test_multi_crack_exemples(context_conseille_3);

}

void probas(uint64_t taille_dictionnaire) {

    Proba testProba(taille_dictionnaire);
    testProba.make();
    testProba.trier();
    for (std::vector<Value>::iterator it = testProba.resultats.begin() ; it != testProba.resultats.end(); ++it) {
        std::cout << (*it).toString() << std::endl;
    }
}

void tests_integration(int argc, char *const *argv) {

    IntegrationTest integrationTest;
    Context context;
    std::string nom_contexte;

    if(strcmp(argv[1], "--creer") == 0) {
        if(argc == 3) {
            choisir_context(atoi(argv[2]), context, nom_contexte);
            integrationTest.table(context, nom_contexte);
        } else {
            std::cout << "usage : ./main --creer <numContext>" << std::endl;
        }
    } else if(strcmp(argv[1], "--creer-multi") == 0) {
        context = Context(6, "abcdefghijklmnopqrstuvwxyz0123", 10000, 160000);
        integrationTest.test_multi_table(context);
    } else if(strcmp(argv[1],"--crack-massive") == 0) {
        if(argc == 3) {
            choisir_context(atoi(argv[2]), context, nom_contexte);
            integrationTest.test_cracker_1000(context, nom_contexte);
        } else {
            std::cout << "usage : ./main --crack-massive <numContext>" << std::endl;
        }
    } else if(strcmp(argv[1],"--crack-example") == 0) {
        if(argc == 3) {
            choisir_context(atoi(argv[2]), context, nom_contexte);
            integrationTest.test_cracker_exemples(context, nom_contexte);
        } else {
            std::cout << "usage : ./main --crack-example <numContext>" << std::endl;
        }
    } else if(strcmp(argv[1],"--crack") == 0) {
        if(argc == 4) {
            choisir_context(atoi(argv[2]), context, nom_contexte);
            integrationTest.cracker(context, nom_contexte, std::string(argv[3]));
        } else {
            std::cout << "usage : ./main --crack <numContext> <hash>" << std::endl;
        }
    }  else if(strcmp(argv[1], "--crack-multi-example") == 0) {
        context = Context(6, "abcdefghijklmnopqrstuvwxyz0123", 10000, 160000);
        integrationTest.test_multi_crack_exemples(context);
    } else if(strcmp(argv[1], "--crack-multi") == 0) {
        if(argc == 3) {
            context = Context(6, "abcdefghijklmnopqrstuvwxyz0123", 10000, 160000);
            integrationTest.multi_crack(context, argv[2]);
        } else {
            std::cout << "usage : ./main --crack-multi <hash>" << std::endl;
        }
    }
}

void afficher_contexts_disponibles() {

    Context smallContext(3, "abcdefghijklm", 50, 100);
    Context mediumContext(3, "abcdefghijklmnopqrstuvwxyz", 500, 100);
    Context bigContext(4, "abcdefghijklmnopqrstuvwxyz", 500, 2000);
    Context autoContext(5, "abcdefghijklmnopqrstuvwxyz0123456789", 0.75);
    Context multiContext(6, "abcdefghijklmnopqrstuvwxyz0123", 10000, 160000);

    std::cout << std::endl << "1 :" << std::endl;
    smallContext.afficher();

    std::cout << std::endl << "2 :" << std::endl;
    mediumContext.afficher();

    std::cout << std::endl << "3 :" << std::endl;
    bigContext.afficher();

    std::cout << std::endl << "4 : (contexte utilisant l'autoparamétrage avec une proba de succès > 0.75)" << std::endl;
    autoContext.afficher();

    std::cout << std::endl << "Moult Tables : (contexte non sélectionnable, utilisé lorsque vous choisissez l'option -multi)" << std::endl;
    multiContext.afficher();
}

void choisir_context(int numero_context, Context &context, std::string &nom_contexte) {

    switch (numero_context) {
        case 1:
            context = Context(3, "abcdefghijklm", 50, 100);
            nom_contexte = "small";
            break;
        case 2:
            context = Context(3, "abcdefghijklmnopqrstuvwxyz", 500, 100);
            nom_contexte = "medium";
            break;
        case 3:
            context = Context(4, "abcdefghijklmnopqrstuvwxyz", 500, 2000);
            nom_contexte = "big";
            break;
        case 4:
            context = Context(5, "abcdefghijklmnopqrstuvwxyz0123456789", 0.75);
            nom_contexte = "auto";
            break;
        default:
            std::cout << "Choix invalide (numero_context doit être entre 1 et 4)" << std::endl;
            assert(false);
    }
}

void tests_unitaires() {

    //TEST DES FONCTIONS DE BASE AVEC RESULTATS DONNES PAR LE PROF DANS LE WIKI

    Context firstTeacherContext(5, "abcdefghijklmnopqrstuvwxyz", 5000, 100);
    Context secondTeacherContext(6, "abcdefghijklmnopqrstuvwxyz", 5000, 100);
    Context thirdTeacherContext(5, "abcdefghijklmnopqrstuvwxyz0123456789", 100000, 50);
    UnitTestTeacher unitTestTeacher;
    unitTestTeacher.test(firstTeacherContext, "teacherValuesCtxt1");
    unitTestTeacher.test(secondTeacherContext, "teacherValuesCtxt2");
    unitTestTeacher.test(thirdTeacherContext, "teacherValuesCtxt3");

    //TEST DES FONCTIONS DE BASE AVEC UNE TABLE A TAILLE HUMAINE

    Context tinyContext(3, "abcd", 5, 64);
    UnitTestTiny unitTestTiny;
    unitTestTiny.testBasicMethods(tinyContext);
    unitTestTiny.testTable(tinyContext);
    unitTestTiny.testCracker(tinyContext);
}