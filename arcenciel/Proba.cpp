/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "Proba.h"

double Proba::proba(uint64_t M, uint64_t T, uint64_t nb_tables) {

    double v,mm;
    mm = M; v = 1;
    for (uint64_t i = 0; i < T; i++) {
        v = v * (1 - mm / taille_dictionnaire);
        mm = taille_dictionnaire * (1 - exp(-mm / taille_dictionnaire));
    }
    double p = 1 - v;

    double q = 1 - p;
    double proba_echec = 1;
    for(uint64_t i = 0; i < nb_tables; i++) {
        proba_echec *= q;
    }
    return 1 - proba_echec;
}

//Fonction utilisée pour qsort
int compareValue(const void* a, const void* b) {
    return (int)((((Value*) a)->proba)*10000 - (((Value*) b)->proba)*10000);
}

// Tri _X suivant indiceT.
void Proba::trier() {
    qsort(this->resultats.data(), this->resultats.size(), sizeof(Value), compareValue);
}

void Proba::incrementer(uint64_t &i) {
    for (uint64_t ordre_grandeur = 100000000; ordre_grandeur > 0; ordre_grandeur /= 10) {
        if (ordre_grandeur < i) {
            i+=ordre_grandeur;
        }
    }
}

void Proba::make() {

    //Avec une table je fais varier M et T
    for (uint64_t M = 100; M < 20000; this->incrementer(M)) {
        for (uint64_t T = 100; T < 10000; this->incrementer(T)) {
            this->resultats.push_back(Value(1, M, T, taille_dictionnaire, this->proba(M, T, 1)));
        }
    }

    //Avec plusieurs tables M est fixé à 20000 par table, je fais seulement varier T
    uint64_t M = 20000;
    for (uint64_t nb_tables = 1; nb_tables < 20; nb_tables++) {
        for (uint64_t T = 100; T < 10000; this->incrementer(T)) {
            this->resultats.push_back(Value(nb_tables, M, T, taille_dictionnaire, this->proba(M, T, nb_tables)));
        }
    }
}

std::string Value::toString() {

    std::ostringstream ss;
    ss << std::setfill('0') << std::setw(2) << nb_tables << " "
        << std::setfill('0') << std::setw(6) << M << " "
        << std::setfill('0') << std::setw(4) << T
        << " " << proba;
    return ss.str();
}

