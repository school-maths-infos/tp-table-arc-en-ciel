/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include <cstdint>
#include <string>
#include <cstdio>
#include <iostream>
#include <cmath>
#include "Empreinte.h"
#include <cstdlib>
#include <openssl/md5.h>

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CONTEXT_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CONTEXT_H


class Context {
public:

    uint64_t taille_dictionnaire;      // nombre de mots
    uint64_t taille_mots;   // nombre de lettres d'un mot
    uint64_t taille_alphabet;   // nombre de lettres possibles pour un caractère
    std::string alphabet;   // tableau des lettres de taille taille_alphabet
    uint64_t T;            // nombre d'étapes (les colonnes seront numérotées de 0 à T)
    uint64_t M;            //nombre de lignes dans la table

    //Constructeurs :

    Context() {}

    // Pour créer un contexte en précisant tous ses paramètres
    Context(uint64_t taille_mots, std::string alphabet, uint64_t T, uint64_t M):
            taille_mots(taille_mots),
            taille_alphabet(alphabet.size()),
            alphabet(alphabet),
            T(T),
            M(M) {

        this->taille_dictionnaire = 1;
        for (uint64_t i = 0; i < taille_mots; i++) {
            this->taille_dictionnaire *= this->taille_alphabet;
        }
    }

    // Pour créer un contexte autoparamétré
    Context(uint64_t taille_mots, std::string alphabet, double probaSuccessDesired) :
            taille_mots(taille_mots),
            taille_alphabet(alphabet.size()),
            alphabet(alphabet) {

        this->taille_dictionnaire = 1;
        for (uint64_t i = 0; i < taille_mots; i++) {
            this->taille_dictionnaire *= this->taille_alphabet;
        }

        this->T = 50; this->M = 100;
        while (proba_succes() < probaSuccessDesired) {
            this->T *= 2;
            this->M = this->T * 10;
        }
    }

    // Transformer un indice en texte clair
    void i2c(uint64_t indice, std::string &clair);
    // Hasher un texte clair
    void h(std::string clair, Empreinte &empreinte);
    // Transformer un hashé en indice
    uint64_t h2i(uint64_t colonne, Empreinte &empreinte, uint64_t numero_table);
    // Transformer un indice en indice : (i2c o h o h2i)^{col_arrivee-col_depart)
    uint64_t i2i(uint64_t colonne_arrivee, uint64_t indice, uint64_t colonne_debut, uint64_t numero_table);
    // Indice aléatoire entre 0 et taille_dictionnaire - 1
    uint64_t indice_aleatoire();
    // Affichage
    void afficher();
    // Calcul des chances de succès (avec 1 seule table)
    double proba_succes();
};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CONTEXT_H
