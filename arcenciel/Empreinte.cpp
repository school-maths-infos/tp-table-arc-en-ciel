/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "Empreinte.h"

//Transformer un digest en chaine de caractères
std::string Empreinte::toString() {

    char buffer[64];
    memset(buffer, 0, sizeof (buffer));
    for(int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(buffer, "%s%02x", buffer, digest[i]);
    }
    return buffer;
}

//Transforme un caractère en valeur numérique (utilisé pour convertir un string en digest)
unsigned char Empreinte::charToChiffre(char c) {

    unsigned char valeurChiffre = 0;

    switch(c) {
        case '0': valeurChiffre = 0;
            break;
        case '1' : valeurChiffre = 1;
            break;
        case '2' : valeurChiffre = 2;
            break;
        case '3' : valeurChiffre = 3;
            break;
        case '4' : valeurChiffre = 4;
            break;
        case '5' : valeurChiffre = 5;
            break;
        case '6' : valeurChiffre = 6;
            break;
        case '7' : valeurChiffre = 7;
            break;
        case '8' : valeurChiffre = 8;
            break;
        case '9' : valeurChiffre = 9;
            break;
        case 'a' : valeurChiffre = 10;
            break;
        case 'b' : valeurChiffre = 11;
            break;
        case 'c' : valeurChiffre = 12;
            break;
        case 'd' : valeurChiffre = 13;
            break;
        case 'e' : valeurChiffre = 14;
            break;
        case 'f' : valeurChiffre = 15;
            break;
    }

    return valeurChiffre;
}

//Transformer une chaine de 32 caractères en digest
void Empreinte::setDigest(std::string hash) {

    for (int i = 0; i < MD5_DIGEST_LENGTH*2; i+=2) {
        digest[i/2] = charToChiffre(hash[i]) * 16 + charToChiffre(hash[i + 1]);
    }
}

//Vérifier l'égalité entre deux condensés
bool Empreinte::equals(Empreinte empreinte) {

    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        if (empreinte.digest[i] != this->digest[i]) {
            return false;
        }
    }
    return true;
}