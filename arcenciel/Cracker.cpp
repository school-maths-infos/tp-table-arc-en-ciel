/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "Cracker.h"

// Idée générale : tester toutes les colonnes A pour le condensé recherché
// Si je note A la colonne dans laquelle se trouve le condensé recherché j'obtiens ceci :
// Col0 --i2i(A,0)--> ColA --i2c--> clair --h--> condense --h2i(A+1)--> Col{A+1} --i2i(T,A+1)--> ColT

// Remarque : le paramètre colonne ne sert à rien.
// Nous l'avons mis car ça nous semblait intéressant d'afficher à quelle colonne on a réussi le crackage

bool Cracker::cracker(Empreinte empreinte, ArcEnCiel arcEnCiel, Context context, std::string &clair, uint64_t &colonne, uint64_t numero_table) {

    //Col 0 --i2i(1,0)--> Col 1 --i2i(2,1)--> Col 2 --i2i(3,2)--> Col 3 --i2i(4,3)--> Col 4 --i2i(5,4)--> Col 5
    //Je vais tester toutes les positions pour mon empreinte :
        // est-il en colonne T-1 ?
        // sinon est-il en colonne T ?
        // et ainsi de suite jusqu'à 0

    for (uint64_t recule = 1; recule <= context.T; recule++) { //A = T-recule

        //Je me place en colonne T-recule. Je suppose que mon empreinte s'y trouve.
        //Je fais h2i pour obtenir l'indice qui le suit directement (en colonne T-recule+1)
        //Je fais i2i pour aller de l'indice en colonne T-recule+1 jusqu'à la fin en colonne T
        uint64_t idxT_potentiel = context.i2i(
                context.T,
                context.h2i(context.T - recule + 1, empreinte, numero_table),
                context.T - recule + 1,
                numero_table
        );

        //Je cherche cet indice en colonne T dans la table arcEnCiel
        uint64_t p,q;
        if (arcEnCiel.rechercher(idxT_potentiel, p, q)) {
            //Si je le trouve, je lance des alertes pour toutes les lignes ou je l'ai trouvé
            for (uint64_t i = p; i <= q; i++) {
                Chaine _X = arcEnCiel.table[i];
                //Maintenant je vais chercher dans cette ligne en repartant du début si je peux trouver le clair qui a donné ce condensé.
                if(this->verifier_alerte(context, recule, _X, clair, empreinte, colonne, numero_table)) {
                    return true;
                }
            }
        }
    }

    return false;
}

bool Cracker::verifier_alerte(Context &context, uint64_t recule, Chaine &chaine, std::string &clair, Empreinte empreinte,
                              uint64_t &colonne, uint64_t numero_table) {

    //Depuis la colonne 0, je fais i2i jusqu'à la colonne T-recule, dans laquelle je pense que le empreinte pourrait se trouver
    uint64_t indice_col_T_moins_recule = context.i2i(context.T - recule, chaine.indice0, 0, numero_table);
    //Depuis l'indice en colonne T-recule, je fais i2c pour avoir le clair et h pour avoir le hashé
    clair.clear();
    context.i2c(indice_col_T_moins_recule, clair);
    //Si je trouve le hashé recherché, c'est que j'avais le bon clair, c'est terminé.
    Empreinte empreinte_trouvee;
    context.h(clair, empreinte_trouvee);
    if (empreinte_trouvee.equals(empreinte)) {
        colonne = context.T - recule + 1;
        return true;
    }
    return false;
}
