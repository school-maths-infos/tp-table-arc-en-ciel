/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "ArcEnCielMulti.h"

//On crée, trie et sauvegarde successivement toutes les tables nécessaires
// (remarque : on arrondit M au multiple de M_PAR_TABLE supérieur)
void ArcEnCielMulti::creer(Context &context, std::string nom_fichier) {

    for(uint64_t numeroTable = 0; numeroTable < nb_tables; numeroTable++) {
        creer_une_table(context, nom_fichier, numeroTable);
    }
}

void ArcEnCielMulti::creer_une_table(const Context &context, const std::string &nom_fichier, uint64_t numero_table) const {

    std::cout << "Création table " << numero_table << " / " << nb_tables - 1 << std::endl;
    ArcEnCiel arcEnCiel(M_PAR_TABLE, context.T);
    arcEnCiel.numero_table = numero_table;
    arcEnCiel.creer(context);
    arcEnCiel.trier();
    std::ostringstream ss;
    ss << numero_table;
    arcEnCiel.sauvegarder(nom_fichier + "_" + ss.str());
}

// Pour aller chercher la prochaine table ( le parcours d'une multitable fonctionnera à peu près comme un itérateur :
//   Pour accéder à toutes les tables de la multitable on fera :
//      while(mamultitable.suivante()) {
//          traitements sur mamultitable.table_courante
//      }
bool ArcEnCielMulti::suivante(Context &context, std::string nom_fichier) {

    if (numero_table_courante < nb_tables) {
        return get_suivante(nom_fichier);
    }
    return false;
}

bool ArcEnCielMulti::get_suivante(const std::string &nom_fichier) {

    table_courante.numero_table = numero_table_courante;
    std::ostringstream ss;
    ss << numero_table_courante;
    table_courante.recharger(nom_fichier + "_" + ss.str());
    numero_table_courante++;
    return true;
}

// Le calcul de probabilité doit se faire avec M = M_PAR_TABLE pour une table
double ArcEnCielMulti::proba_succes_par_table(uint64_t T, uint64_t nb_mots) {

    double v,mm;
    mm = M_PAR_TABLE; v = 1;
    for (uint64_t i = 0; i < T; i++) {
        v = v * (1 - mm / nb_mots);
        mm = nb_mots * (1 - exp(-mm / nb_mots));
    }
    return 1 - v;
}

// Le calcul de probabilité pour l'ensemble se fait en utilisant Bernouilli
// (on veut au moins 1 succès => le contraire de "que des échecs")
double ArcEnCielMulti::proba_succes(Context &context) {

    double q = 1 - proba_succes_par_table(context.T, context.taille_dictionnaire);
    double proba_echec = 1;
    for(uint64_t i = 0; i < nb_tables; i++) {
        proba_echec *= q;
    }
    return 1 - proba_echec;
}