/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include <cmath>
#include <cstdint>
#include <vector>
#include <cstdlib>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_TESTPROBA_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_TESTPROBA_H

struct Value {

    uint64_t nb_tables;
    uint64_t M;
    uint64_t T;
    double proba;

    Value(uint64_t nb_tables, uint64_t M, uint64_t T, uint64_t taille_dictionnaire, double proba) :
            nb_tables(nb_tables), M(M), T(T), proba(proba) {}

    std::string toString();
};

class Proba {
public:
    std::vector<Value> resultats;
    uint64_t taille_dictionnaire;

    Proba(uint64_t taille_dictionnaire) : taille_dictionnaire(taille_dictionnaire) {}

    double proba(uint64_t M, uint64_t T, uint64_t nb_tables);
    void make();
    void trier();
    void incrementer(uint64_t &i);
};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_TESTPROBA_H
