/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "Context.h"
#include "ArcEnCiel.h"

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CRACKER_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CRACKER_H

class Cracker {
public:
    // Retourne vrai si l'alerte était bonne, c est alors le texte clair
    // correspondant à l'empreinte y.
    bool verifier_alerte(Context &context, uint64_t recule, Chaine &chaine, std::string &clair, Empreinte empreinte,
                         uint64_t &colonne, uint64_t numero_table);
    // Essaye de cracker l'empreinte
    bool cracker(Empreinte empreinte, ArcEnCiel arcEnCiel, Context context, std::string & clair, uint64_t &colonne, uint64_t numero_table);

};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CRACKER_H
