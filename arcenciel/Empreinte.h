/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */
#include <openssl/md5.h>
#include <string>
#include <string.h> //nécessaire pour memset

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CONDENSE_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CONDENSE_H



class Empreinte {
public:
    //Le tableau de 16 nombres en hexadécimal rendu par la fonction MD5 d'openssl
    unsigned char digest[MD5_DIGEST_LENGTH];
    //Transformer une chaine de caractères en digest
    void setDigest(std::string hash);
    //Vérifier l'égalité entre deux condensés
    bool equals(Empreinte empreinte);
    //Transformer un digest en chaine de caractères
    std::string toString();

private:
    //Transforme un caractère en valeur numérique (utilisé pour convertir un string en digest
    unsigned char charToChiffre(char c);
};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_CONDENSE_H
