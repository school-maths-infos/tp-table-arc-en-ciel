/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "ArcEnCiel.h"

// Creer les M chaînes de taille T
void ArcEnCiel::creer(Context context) {
    for (uint64_t i = 0; i < M; i++) {
        //Remarque : on part de la colonne 0 à la colonne T et non de 1 à T
        uint64_t idx0 = context.indice_aleatoire();
        uint64_t idxT = context.i2i(context.T, idx0, 0, this->numero_table);
        table[i].indice0 = idx0;
        table[i].indiceT = idxT;
    }
}

//Fonction utilisée pour qsort
int compare(const void* a, const void* b) {
    return (((Chaine*) a)->indiceT - ((Chaine*) b)->indiceT);
}

// Tri _X suivant indiceT.
void ArcEnCiel::trier() {
    qsort(this->table.data(), this->M, sizeof(Chaine), compare);
}


// Sauvegarde la table sur disque.
void ArcEnCiel::sauvegarder(std::string nom_fichier) {

    std::ofstream fichier(nom_fichier.c_str(), std::ofstream::out | std::ofstream::trunc);
    if(fichier) {

        for (uint64_t i = 0; i < M; i++) {
            fichier << table[i].indice0 << " " << table[i].indiceT << std::endl;
        }

        fichier.close();
    }
    else {
        std::cerr << "Impossible d'ouvrir le fichier " << nom_fichier << std::endl;
    }
}

// Charge en mémoire la table à partir du disque.
void ArcEnCiel::recharger(std::string nom_fichier) {

    std::ifstream fichier(nom_fichier.c_str(), std::ifstream::in);
    if(fichier) {

        for (uint64_t i = 0; i < M; i++) {
            fichier >> table[i].indice0 >> table[i].indiceT;
        }

        fichier.close();
    }
    else {
        std::cerr << "Impossible d'ouvrir le fichier " << nom_fichier << std::endl;
    }
}

// Recherche dichotomique dans la table
// ( p et q sont le premier/dernier trouvé )
bool ArcEnCiel::rechercher(uint64_t valeur_cherchee, uint64_t &indice_premier, uint64_t &indice_dernier) {

    bool trouve = false;
    indice_premier = 0;
    indice_dernier = M;
    uint64_t indice_milieu = (indice_premier + indice_dernier) / 2;

    while (!trouve && (indice_dernier - indice_premier) > 1) {
        if (valeur_cherchee < table[indice_milieu].indiceT) {
            indice_dernier = indice_milieu;
        } else {
            indice_premier = indice_milieu;
        }

        indice_milieu = (indice_premier + indice_dernier) / 2;
        trouve = (table[indice_milieu].indiceT == valeur_cherchee);
    }

    indice_premier = 0;
    indice_dernier = 0;
    if (trouve) {
        bool isIdx = true;
        int i = indice_milieu;
        while (isIdx && indice_premier != ((uint64_t) 0 - 1)) {
            isIdx = table[i].indiceT == valeur_cherchee;
            indice_premier = i;
            i--;
        }

        isIdx = true;
        i = indice_milieu;
        while (isIdx && indice_dernier != M) {
            isIdx = table[i].indiceT == valeur_cherchee;
            indice_dernier = i;
            i++;
        }

        indice_premier += 1;
        indice_dernier -= 1;
    }

    return trouve;
}

// Afficher la table (utile lors d'un débuggage)
void ArcEnCiel::afficher() {
    for (uint64_t i = 0; i < M; i++) {
        std::cout << table[i].indice0 << " - " << table[i].indiceT << std::endl;
    }
}