/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "Context.h"

// In: index indice ----> Out: Clair clair
// On voit le mot comme un nombre écrit en base nbLettresDansAlphabet
// On le convertit en base 10 (à l'envers)
void Context::i2c(uint64_t indice, std::string &clair) {

    uint64_t reste; uint64_t quotient = indice;
    for(int i = taille_mots - 1; i >= 0; i--) {
        reste = quotient % taille_alphabet;
        quotient = quotient / taille_alphabet;
        clair.push_back(alphabet[reste]);
    }
}

// fonction de hachage
// In: Clair c ----> Out: Empreinte
void Context::h(std::string clair, Empreinte &empreinte) {
    unsigned char* paramKey = (unsigned char*) clair.c_str();
    MD5(paramKey, clair.size(), empreinte.digest);
}

// On prend les 8 chiffres de poids fort (en base hexadécimale) du hash
// On le convertit en base 10, en ajoutant un modificateur selon la colonne et le numéro de table (pour favoriser l'injectivité)
// On se ramène entre 0 et nbMots-1 par modulo
uint64_t Context::h2i(uint64_t colonne, Empreinte &empreinte, uint64_t numero_table) {
    uint64_t *y = (uint64_t*) empreinte.digest;
    return ((*y) + colonne + 65536 * numero_table) % taille_dictionnaire;
}

// On réalise la fonction g : i2c o h o h2i plusieurs fois en partant de la colonne de départ vers la colonne d'arrivée
// On précise également le numéro de la table pour appliquer les bons biais à h2i
uint64_t Context::i2i(uint64_t colonne_arrivee, uint64_t indice, uint64_t colonne_debut, uint64_t numero_table) {

    std::string clair;
    Empreinte empreinte;
    for (uint64_t i = colonne_debut; i < colonne_arrivee; i++) {
        clair.clear();
        this->i2c(indice, clair);
        this->h(clair, empreinte);
        indice = this->h2i(i + 1, empreinte, numero_table);
    }
    return indice;
}

// Retourne un indice aléatoire valide.
uint64_t Context::indice_aleatoire() {
    unsigned long n1 = random();
    unsigned long n2 = random();
    return ( ( (uint64_t) n2 )
             +  (( (uint64_t) n1 ) << 32) ) % this->taille_dictionnaire;
}

// Affichage du contexte
void Context::afficher() {
    std::cout << "mots de taille " << this->taille_mots << " dans l'alphabet " << this->alphabet << std::endl;
    std::cout << "nombre de mots : " << this->taille_dictionnaire << std::endl;
    std::cout << "paramètres de crackage : M = " << this->M << " et T = " << this->T << std::endl;
}

// Probabilité de succès (avec 1 seule table)
double Context::proba_succes() {

    double v,mm;
    mm = this->M; v = 1;
    for (uint64_t i = 0; i < this->T; i++) {
        v = v * (1 - mm / this->taille_dictionnaire);
        mm = this->taille_dictionnaire * (1 - exp(-mm / this->taille_dictionnaire));
    }
    return 1 - v;
}
