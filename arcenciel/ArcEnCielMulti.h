/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "ArcEnCiel.h"
#include <sstream>

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_ARCENCIELMULTI_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_ARCENCIELMULTI_H
#define M_PAR_TABLE 20000

class ArcEnCielMulti {
public:
    //On gère une table arcEnCiel à la fois (la table table_courante, dont le numéro est numero_table_courante
    uint64_t numero_table_courante;
    uint64_t nb_tables;
    ArcEnCiel table_courante;

    ArcEnCielMulti(Context &context) :
            numero_table_courante(0),
            nb_tables(ceil(context.M / (double) M_PAR_TABLE)), //rq : ca nous fera un peu plus d'éléments car on arrondit
            table_courante(M_PAR_TABLE, context.T)
    {}

    //Création, tri et enregistrement des tables
    void creer(Context &context, std::string nom_fichier);
    //Récupérer la prochaine table (retourne faux si pas de prochaine table)
    bool suivante(Context &context, std::string nom_fichier);
    //Calcule la proba de réussir en tenant compte du multi-table (Bernouilli)
    double proba_succes(Context &context);

private:
    //Création, trie et enregistrement de la table numéro numeroTable
    void creer_une_table(const Context &context, const std::string &nom_fichier, uint64_t numero_table) const;
    //Récupération et chargement de la table suivante
    bool get_suivante(const std::string &nom_fichier);
    //Calcule la proba de succès pour une table
    double proba_succes_par_table(uint64_t T, uint64_t nb_mots);
};

#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_ARCENCIELMULTI_H
