/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include <iostream>
#include <cstdint>
#include <string>
#include <cstdio>
#include <fstream>
#include <vector>
#include "Context.h"

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_ARCENCIEL_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_ARCENCIEL_H

struct Chaine {
    uint64_t indice0;    // premier indice de la chaine
    uint64_t indiceT;    // dernier indice de la chaine
};
// Comparer deux chaines
int compare(const void* a, const void* b);

class ArcEnCiel {
public:

    uint64_t numero_table;   // numero de la table (ici 0, mais voir "Moult tables")
    uint64_t M;              // nombre de chaines dans la table
    uint64_t T;              // longueur de la chaine
    std::vector<Chaine> table;            // la table elle-meme

    ArcEnCiel(uint64_t M, uint64_t T) : numero_table(0), M(M), T(T), table(M) {}

    // Creer les M chaînes de taille T, dans le contexte ctxt
    void creer(Context context);
    // Tri _X suivant indiceT.
    void trier();
    // Sauvegarde la table sur disque.
    void sauvegarder(std::string nom_fichier);
    // Charge en mémoire la table à partir du disque.
    void recharger(std::string nom_fichier);
    // Recherche dichotomique dans la table
    // ( p et q sont le premier/dernier trouvé )
    bool rechercher(uint64_t valeur_cherchee, uint64_t &indice_premier, uint64_t &indice_dernier);
    // Afficher la table
    void afficher();

};

#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_ARCENCIEL_H
