# ajoutez vos programmes ci-dessous
PROGS_SRC= main.cpp
SRC=arcenciel/Empreinte.cpp arcenciel/Context.cpp arcenciel/ArcEnCiel.cpp arcenciel/Cracker.cpp arcenciel/ArcEnCielMulti.cpp arcenciel/Proba.cpp
TEST=tests/IntegrationTest.cpp tests/UnitTestTiny.cpp tests/UnitTestTeacher.cpp
OBJ=${SRC:arcenciel/%.cpp=arcenciel/%.o}
OBJO=${OBJ:arcenciel/%.o=%.o}
OBJTEST=${TEST:tests/%.cpp=tests/%.o}
OBJTESTO=${OBJTEST:tests/%.o=%.o}
HEADERS=arcenciel/Empreinte.h arcenciel/Context.h arcenciel/ArcEnCiel.h arcenciel/Cracker.h arcenciel/ArcEnCielMulti.h tests/IntegrationTest.h tests/UnitTestTiny.h tests/UnitTestTeacher.h arcenciel/Proba.h
PROGS=${PROGS_SRC:.cpp=}
CXXFLAGS=-O3 -g -Wall -std=c++11
LIBS=-lcrypto

all: clean createdir ${PROGS}

createdir:
	mkdir generatedTables

main: main.cpp ${OBJ} ${OBJTEST} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJO} ${OBJTESTO} -o $@ ${LIBS}

arcenciel/%.o: arcenciel/%.cpp arcenciel/%.h
	g++ ${CXXFLAGS} -c $<

tests/%.o: tests/%.cpp tests/%.h
	g++ ${CXXFLAGS} -c $<

clean:
	rm -f -r ${PROGS} ${OBJO} ${OBJTESTO} generatedTables/