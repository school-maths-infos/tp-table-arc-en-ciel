/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

---------------------------------------------------------------------------------------------------------
| Exécution du programme :                                                                              |
---------------------------------------------------------------------------------------------------------

Faites make pour compiler.
exécutez ./main => cela vous donne toutes les options et contextes disponibles :

    ./main --unitaires                  : tests unitaires
    ./main --probas <tailleDico>        : probas
    ./main --probas-test                : des essais avec le contexte 4 paramétré avec les conseils de la classe Proba
    ./main --creer <numContext>         : création de la table pour le contexte <numContext>
    ./main --creer-multi                : création de plusieurs tables (partie Moult Tables)
    ./main --crack-massive <numContext> : crackage de 1000 hashés pour le contexte <numContext>
    ./main --crack-example <numContext> : crackage de 10 hashés pour le contexte <numContext>
    ./main --crack <numContext> <hash>  : crackage de <hash> pour le contexte <numContext>
    ./main --crack-multi-example        : crackage de 10 hashés (partie Moult Tables)
    ./main --crack-multi <hash>         : crackage de <hash> (partie Moult Tables)

---------------------------------------------------------------------------------------------------------
| Organisation du projet :                                                                              |
---------------------------------------------------------------------------------------------------------
-racine du dossier
    - Makefile et programme main
    - arcenciel : dossier contenant notre code c++
        - classes Empreinte, Context, ArcEnCiel, ArcEnCielMulti et Cracker
    - generatedTables : les tables sauvegardées par ArcEnCiel et ArcEnCielMulti sont rangées là
    - tests : quelques classes pour organiser nos tests :
        - Tests unitaires sur un exemple à taille humaine (64 mots)
        - Tests unitaires basés sur les résultats de référence donnés dans l'énoncé
        - Tests d'intégration
        - datas : Les valeurs de référence que nos tests utiliseront
    - readme.txt : ce ficher

---------------------------------------------------------------------------------------------------------
| Quelques statistiques :                                                                               |
---------------------------------------------------------------------------------------------------------

nbTables | nbMots   | M           | T    | creationTable | crackage | succès estimé | succès observé
Avec une table les essais ont été faits sur un ensemble de 1000 crackages
1        | 2197     | 100         | 50   | O  s          | 0    s   | 78.9 %        | 81 %
1        | 17576    | 100         | 500  | 0  s          | 0.03 s   | 83.0 %        | 82 %
1        | 456976   | 2000        | 500  | 0  s          | 0.03 s   | 77.3 %        | 80 %
1        | 60466176 | 640000      | 6400 | 94 s          | 7.7  s   | 94.8 %        | 94 %
-----------------------------------------------------------------------------------------------------
En multi table les essais ont été faits sur un ensemble de 10 crackages -> pas très représentatif
12       | 60466176 | 20000/Table | 333  | 22 s          | 0    s   | 75.2 %        | 8 / 10
8        | 60466176 | 20000/Table | 555  | 24 s          | 0.2  s   | 75.5 %        | 7 / 10
10       | 60466176 | 20000/Table | 444  | 35 s          | 0.1  s   | 75.8 %        | 9 / 10
-----------------------------------------------------------------------------------------------------
2        | 60466176 | 20000/Table | 6665 | 81 s          | 6.6  s   | 94.9 %        | 9 / 10
12       | 60466176 | 20000/Table | 777  | 45 s          | 0.3  s   | 94.5 %        | 9 / 10
14       | 60466176 | 20000/Table | 666  | 45 s          | 0.2  s   | 94.6 %        | 9 / 10
17       | 60466176 | 20000/Table | 555  | 51 s          | 0.2  s   | 94.9 %        | 10 / 10

---------------------------------------------------------------------------------------------------------
| Fonctionnalités proposées :                                                                           |
---------------------------------------------------------------------------------------------------------

Fonctions i2c, h, h2i et i2i :
---------------------------------------------------------------------------------------------------------
    Ces fonctions sont implémentées dans la classe Context et testées unitairement.

    Des tests sont effectués par rapport aux exemples de référence donnés dans l'énoncé
    Des tests sont effectués par rapport à un exemple de référence à taille humaine (64 mots)
     que nous avons construit nous même.

        ./main --unitaires

Création de table et fonctionnalités associées (trier, sauvegarder, recharger, rechercher)
---------------------------------------------------------------------------------------------------------
    Ces fonctions sont implémentées dans la classe ArcEnCiel.
    On se limite à des dictionnaire contenant des mots de même taille.
    Si on voulait travailler avec plusieurs tailles de mots, il faudrait un contexte et une table par taille.

    Elles peuvent être testées unitairement sur notre exemple de 64 mots.

        ./main --unitaires

    Elles peuvent être testées sur les 4 contextes que nous proposons dans le programme ./main :

        ./main --create <numContext[1-4]>

Moult tables
---------------------------------------------------------------------------------------------------------
    La classe ArcEnCielMulti permet d'utiliser plusieurs tables (la constante M_PAR_TABLE défini la taille des tables).

    La méthode creer() crée, trie et sauvegarde toutes les tables nécéssaires (on arrondi M au multiple de M_PAR_TABLE supérieur)

        ./main --creer-multi

    Pour utiliser les tables, il faut ensuite utiliser la fonction next(), qui va recharger successivement les tables

Crackage d'un hashé MD5
---------------------------------------------------------------------------------------------------------
    La classe cracker permet de retrouver le clair à partir d'un hashé,
    à condition qu'il se trouve quelque part dans la table

    Il peut être testé unitairement sur notre exemple de 64 mots :

        ./main --unitaires

    Pour cracker un hash donné

        ./main --crack <numContext[1-4]> <hash>

    Pour voir quelques exemples (10 hash sont choisis au hasard et crackés)

        ./main --crack-example <numContext[1-4]>

    Pour voir le taux de succès, on calcule le taux de succès estimé (fonction proba_succes du contexte).
    Puis on effectue 1000 crackage sur des hash choisis aléatoirement et on observe le taux de réussite.
    Généralement c'est plutôt proche du taux de succès estimé

        ./main --crack-massive <numContext[1-3]> //Possible aussi avec le 4, mais très long

    Nous pouvons paramétrer automatiquement un contexte pour avoir un taux de succès minimal donné.
    Nous donnons l'alphabet et le nombre de lettres par mot, et le contexte choisi lui-même T et M.
    Parmi nos contextes d'exemple, le numéro 4 a été créé ainsi.
    Nous pouvons remarquer que notre façon de définir M et T n'est pas forcément idéale.
    En effet, Avec notre méthode on fait T=T*2, M=T*1000 tant que proba < probaDemandée.
    Dans le contexte 4, on demande 0.75.
    A l'étape T = 3200 et M = 32000 nous avons une proba < 0.75.
    On se retrouve à l'étape suivante avec T = 6400, M = 64000 et proba = 94,81 (c'est trop).
    L'idéal serait d'étudier les variations de la fonction proba(nbMots, T, M) afin de faire varier T et M d'une manière plus adéquate.
    On pourrait aussi aller plus loin en choisissant s'il vaut mieux utiliser une table simple ou une multi-table.
    Par exemple, dès que M serait supérieur à 20000, on déciderait d'utiliser une multi-table et on tiendrait compte des probas calculées en multi-table.
    Nous avons commencé : création d'une table de probabilités (nb_tables en 1ere colonne, M en 2eme colonne, T en 3eme colonne, proba en 4eme colonne)

        ./main --probas <taille_du_dictionnaire> > tests/datas/probas

    Pour notre exemple du contexte 4, pour une proba de 0.75 on nous propose :
        13 020000 0333 0.751962
        08 020000 0555 0.754726
        10 020000 0444 0.757683
    Nous allons essayer ces 3 contextes et voir nos temps d'exécution (précédemment nous avions 7.7 secondes en moyenne par crackage)

        ./main --probas-test

    Pour notre exemple du contexte 4, nous pouvons aussi voir l'efficacité du multi table en prenant les contextes conseillés pour la même proba de 0.94 :
            12 020000 0777 0.945089
            14 020000 0666 0.94641
            02 020000 6665 0.948827
            17 020000 0555 0.949533
    Nous allons essayer ces 4 contextes et voir nos temps d'exécution (précédemment nous avions 7.7 secondes en moyenne par crackage)

        ./main --probas-test-94

    Pour cracker un hash en utilisant Moult tables, il faut :
     Créer les tables.
     Faire next() et essayer de cracker avec la table obtenue
     Recommencer suivante() et essayer de cracker et ainsi de suite
     Jusqu'à avoir trouvé ou avoir épuisé toutes les tables

        ./main --crack-multi <hash>

    La probabilité de succès est calculée par la classe ArcEnCielMulti :
     On voit le crackage avec une table comme une expérience de Bernouilli :
       p est calculé avec la méthode vue précédemment pour une seule table
       Réussir est le contraire de "échouer sur toutes les tables"
       La proba de succès est donc 1 - q^{nb_tables}

        ./main --crack-multi-example
        (le crack-massif n'est pas proposé car trop long, même s'il serait mieux pour vérifier la proba de succès)
