/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include <sstream>
#include <assert.h>
#include "IntegrationTest.h"
#include "../arcenciel/ArcEnCiel.h"
#include "../arcenciel/Cracker.h"
#include "../arcenciel/ArcEnCielMulti.h"

void IntegrationTest::table(Context &context, std::string nom_context) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_" << nom_context;
    std::cout << "Création de la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    ArcEnCiel arcEnCiel(context.M, context.T);
    time_t tbegin = time(NULL);
    arcEnCiel.creer(context);
    time_t tcreate = time(NULL);
    std::cout << "Durée de création de la table : " << difftime(tcreate,tbegin) << " secondes" << std::endl;
    arcEnCiel.trier();
    time_t ttri = time(NULL);
    std::cout << "Durée de tri de la table : " << difftime(ttri,tcreate) << " secondes" << std::endl;
    std::cout << "Durée Totale : " << difftime(ttri,tbegin) << " secondes" << std::endl;

    uint64_t idxT = 0;
    uint64_t uniques = 0;
    for (uint64_t i = 0; i < context.M; i++) {
        if (arcEnCiel.table[i].indiceT > idxT) {
            uniques++;
        }
    }

    arcEnCiel.sauvegarder(ss.str());

    std::cout << "Terminé (valeurs uniques dans la table : " << (uniques / context.M) * 100 << " %)" << std::endl << std::endl;
}

void IntegrationTest::test_cracker_1000(Context &context, std::string nom_context) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_" << nom_context;
    std::cout << "Crackage avec la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    std::cout << "Chances de succès estimées : " << context.proba_succes() * 100 << "%" << std::endl;

    ArcEnCiel arcEnCiel(context.M, context.T);
    arcEnCiel.recharger(ss.str());

    Cracker cracker;
    srand(time(NULL));
    uint64_t nbSuccess = 0;
    time_t tbegin = time(NULL);
    for (uint64_t i = 0; i < 1000; i++) {
        unsigned long n1 = random();
        unsigned long n2 = random();
        uint64_t index = ((uint64_t) n2) +  (( (uint64_t) n1 ) << 32) % context.taille_dictionnaire;

        Empreinte empreinte;
        std::string clair_expected;
        context.i2c(index, clair_expected);
        context.h(clair_expected, empreinte);
        std::string clair_actual;
        uint64_t colonne;
        if(cracker.cracker(empreinte, arcEnCiel, context, clair_actual, colonne, 0)) {
            nbSuccess++;
            assert(clair_actual == clair_expected);
        }
        std::cout << "." << std::flush;
    }
    time_t tend = time(NULL);
    std::cout << std::endl << "Temps de crackage moyen : " << difftime(tend,tbegin) / 1000 << " secondes" << std::endl;

    std::cout << std::endl << "Terminé (Taux de succès observé : " << (nbSuccess/10) << "%)" << std::endl << std::endl;
}

void IntegrationTest::test_cracker_exemples(Context &context, std::string nom_context) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_" << nom_context;
    std::cout << "Crackage avec la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    std::cout << "Chances de succès estimées : " << context.proba_succes() * 100 << "%" << std::endl;

    ArcEnCiel arcEnCiel(context.M, context.T);
    arcEnCiel.recharger(ss.str());

    Cracker cracker;
    srand(time(NULL));
    uint64_t nbSuccess = 0;
    for (uint64_t i = 0; i < 10; i++) {
        unsigned long n1 = random();
        unsigned long n2 = random();
        uint64_t index = ((uint64_t) n2) +  (( (uint64_t) n1 ) << 32) % context.taille_dictionnaire;

        Empreinte empreinte;
        std::string clair_expected;
        context.i2c(index, clair_expected);
        context.h(clair_expected, empreinte);
        std::string clair_actual;
        uint64_t colonne;

        std::cout << "Exemple " << i << " motClair = " << clair_expected << " hash = " << empreinte.toString() << std::endl;
        if(cracker.cracker(empreinte, arcEnCiel, context, clair_actual, colonne, 0)) {
            nbSuccess++;
            std::cout << "Crackage réussi en colonne " << colonne << " Mot Trouvé : " << clair_actual << std::endl;
            assert(clair_actual == clair_expected);
        } else {
            std::cout << "Mot clair non trouvé" << std::endl;
        }
    }

    std::cout << "Terminé" << std::endl << std::endl;
}

void IntegrationTest::cracker(Context &context, std::string nom_context, std::string hash) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_" << nom_context;
    std::cout << "Crackage avec la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    std::cout << "Chances de succès estimées : " << context.proba_succes() * 100 << "%" << std::endl;

    ArcEnCiel arcEnCiel(context.M, context.T);
    arcEnCiel.recharger(ss.str());

    Cracker cracker;

    Empreinte empreinte;
    empreinte.setDigest(hash);
    std::string clair;
    uint64_t colonne;

    std::cout << "Crackage de " << empreinte.toString() << std::endl;
    if(cracker.cracker(empreinte, arcEnCiel, context, clair, colonne, 0)) {
        std::cout << "Crackage réussi en colonne " << colonne << " Mot Trouvé : " << clair << std::endl;
    } else {
        std::cout << "Mot clair non trouvé" << std::endl;
    }

    std::cout << "Terminé" << std::endl << std::endl;
}

void IntegrationTest::test_multi_table(Context &context) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_multi";
    std::cout << "Crackage avec la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    ArcEnCielMulti arcEnCiel(context);
    time_t tbegin = time(NULL);
    arcEnCiel.creer(context, ss.str());
    time_t tend = time(NULL);
    std::cout << "Terminé en " << difftime(tend,tbegin) << " secondes" << std::endl << std::endl;

}

void IntegrationTest::test_multi_crack_exemples(Context &context) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_multi";
    std::cout << "Crackage avec la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    ArcEnCielMulti arcEnCiel(context);

    std::cout << "Chances de succès estimées : " << arcEnCiel.proba_succes(context) * 100 << "%" << std::endl;

    Cracker cracker;
    srand(time(NULL));
    uint64_t nbSuccess = 0;
    time_t tbegin = time(NULL);
    for (uint64_t i = 0; i < 10; i++) {
        unsigned long n1 = random();
        unsigned long n2 = random();
        uint64_t index = ((uint64_t) n2) +  (( (uint64_t) n1 ) << 32) % context.taille_dictionnaire;

        ArcEnCielMulti arcEnCiel(context);
        Empreinte empreinte;
        std::string clair_expected;
        context.i2c(index, clair_expected);
        context.h(clair_expected, empreinte);
        std::string clair_actual;
        uint64_t colonne;

        std::cout << "Exemple " << i << " motClair = " << clair_expected << " hash = " << empreinte.toString() << std::endl;
        bool trouve = false;
        while (arcEnCiel.suivante(context, ss.str())) {
            if (cracker.cracker(empreinte, arcEnCiel.table_courante, context, clair_actual, colonne,
                                arcEnCiel.numero_table_courante - 1)) {
                trouve = true;
                nbSuccess++;
                std::cout << "Crackage réussi en table " << (arcEnCiel.numero_table_courante - 1) << " colonne " << colonne << " Mot Trouvé : " <<
                clair_actual << std::endl;
                assert(clair_actual == clair_expected);
                break;
            } else {
                std::cout << "." << std::flush;
            }
        }
        if(!trouve) {
            std::cout << "Non trouvé, toutes les tables ont échoué" << std::endl;
        }

    }

    time_t tend = time(NULL);

    std::cout << "Terminé. Temps moyen de crackage (sur 10 essais) : " << difftime(tend,tbegin) / 10 << std::endl << std::endl;

}

void IntegrationTest::multi_crack(Context &context, std::string hash) {

    std::stringstream ss;
    ss << "generatedTables/tableT" << context.T << "M" << context.M << "_multi";
    std::cout << "Crackage avec la table " << ss.str() << " dans le context : " << std::endl;
    context.afficher();

    ArcEnCielMulti arcEnCiel(context);
    double probaEchec = 1;
    double q = 1.0- context.proba_succes();
    for (uint64_t i = 0; i < arcEnCiel.nb_tables; i++) {
        probaEchec *= q;
    }

    std::cout << "Chances de succès estimées : " << (1.0 - probaEchec)*100 << "%" << std::endl;

    Cracker cracker;
    Empreinte empreinte;
    empreinte.setDigest(hash);
    std::string clair;
    uint64_t colonne;

    std::cout << "Crackage de " << empreinte.toString() << std::endl;
    while (arcEnCiel.suivante(context, ss.str())) {
        if (cracker.cracker(empreinte, arcEnCiel.table_courante, context, clair, colonne,
                            arcEnCiel.numero_table_courante - 1)) {
            std::cout << "Crackage réussi en table " << (arcEnCiel.numero_table_courante - 1) << " colonne " << colonne << " Mot Trouvé : " <<
            clair << std::endl;
            break;
        } else {
            std::cout << "non trouvé dans la table " << (arcEnCiel.numero_table_courante - 1) << std::endl;
        }
    }

    std::cout << "Terminé" << std::endl << std::endl;

}
