/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include <fstream>
#include <assert.h>
#include <iomanip>
#include <string.h>
#include "../arcenciel/Cracker.h"
#include "../arcenciel/Context.h"
#include "../arcenciel/ArcEnCiel.h"

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_UNITTEST64_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_UNITTEST64_H

class UnitTestTiny {
public:
    bool verbose;

    UnitTestTiny() : verbose(false) {}
    UnitTestTiny(bool pverbose) : verbose(pverbose) {}

    void testBasicMethods(Context &context);
    void testTable(Context &context);
    void testCracker(Context &context);
};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_UNITTEST64_H
