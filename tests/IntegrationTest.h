/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_INTEGRATIONTEST_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_INTEGRATIONTEST_H


#include "../arcenciel/Context.h"

class IntegrationTest {
public:
    void table(Context &context, std::string nom_context);
    void test_cracker_1000(Context &context, std::string nom_context);
    void test_cracker_exemples(Context &context, std::string nom_context);
    void test_multi_crack_exemples(Context &context);
    void test_multi_table(Context &context);
    void cracker(Context &context, std::string nom_context, std::string hash);
    void multi_crack(Context &context, std::string hash);
};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_INTEGRATIONTEST_H
