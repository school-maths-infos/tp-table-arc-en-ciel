/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "../arcenciel/Context.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <assert.h>

#ifndef INFO006_ARCENCIEL_LECLAIRE_DEROLAND_UNITTESTTEACHER_H
#define INFO006_ARCENCIEL_LECLAIRE_DEROLAND_UNITTESTTEACHER_H


class UnitTestTeacher {

public:
    void test(Context &context, std::string nom_fichier);
    bool verbose;
    UnitTestTeacher() : verbose(false) {}
    UnitTestTeacher(bool pverbose) : verbose(pverbose) {}
};


#endif //INFO006_ARCENCIEL_LECLAIRE_DEROLAND_UNITTESTTEACHER_H
