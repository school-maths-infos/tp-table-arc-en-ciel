/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "UnitTestTiny.h"

void UnitTestTiny::testBasicMethods(Context &context) {

    std::ifstream input("tests/datas/plainTable64", std::ifstream::in);
    if (input) {
        uint64_t indices_actual[context.T + 1];
        uint64_t indices_expected[context.T + 1];
        std::string clair_actual;
        std::string clair_expected;
        std::string hash_expected;
        Empreinte condense_actual;
        Empreinte condense_expected;

        std::cout << "Test des méthodes h2i, i2c et i2i sur 64 indices" << std::endl;

        for (uint64_t i = 0; i < context.taille_dictionnaire; i++) {

            clair_actual.clear();
            indices_actual[0] = i;
            input >> indices_expected[0];
            if (this->verbose)
                std::cout << indices_actual[0];

            for (uint64_t k = 1; k <= context.T; k++) {
                clair_actual.clear();
                clair_expected.clear();
                hash_expected.clear();
                //Calcul des valeurs obtenues
                /*i2c*/     context.i2c(indices_actual[k-1], clair_actual);
                /*h*/       context.h(clair_actual, condense_actual);
                /*h2i(i)*/  indices_actual[k] = context.h2i(k, condense_actual, 0);
                //Lecture des valeurs attendues
                input >> clair_expected >> hash_expected >> indices_expected[k];
                condense_expected.setDigest(hash_expected);
                //Vérifications
                if (verbose)
                    std::cout << " " << clair_actual << " " << condense_actual.toString() << " " << indices_actual[k];
                assert(clair_actual == clair_expected);
                assert(condense_actual.equals(condense_expected));
                assert(indices_actual[k] == indices_expected[k]);
                for (uint64_t n = 0; n < k; n++) {
                    assert(context.i2i(k,indices_actual[n],n,0) == indices_expected[k]);
                }
            }
            if (verbose)
                std::cout << std::endl;
            else
                std::cout << ".";
        }

        std::cout << std::endl << "Test des méthodes de base réussi" << std::endl;
        input.close();
    }
    else {
        std::cerr << "Impossible d'ouvrir le fichier tests/datas/plainTable64 !" << std::endl;
        assert(false);
    }
}

void UnitTestTiny::testTable(Context &context) {

    std::cout << std::endl << "Test de la table arc en ciel" << std::endl;

    //Je cherche les indices dans le fichier de test
    std::ifstream file("tests/datas/shortTable64", std::ifstream::in);
    uint64_t indices_col_T[context.taille_dictionnaire];
    if (file) {
        uint64_t idxT, idx0;
        for (uint64_t i = 0; i < context.taille_dictionnaire; i++) {
            file >> idx0 >> idxT;
            indices_col_T[i] = idxT;
        }
        file.close();
    }
    else {
        std::cerr << "Impossible d'ouvrir le fichier !" << std::endl;
        assert(false);
    }

    //Je crée la table
    std::cout << "Création de la table ... ";
    ArcEnCiel arcEnCiel(context.M, context.T);
    arcEnCiel.creer(context);

    //Je vérifie que les valeurs sont justes
    for (uint64_t i = 0; i < 64; i++) {
        assert(arcEnCiel.table[i].indiceT == indices_col_T[arcEnCiel.table[i].indice0]);
    }
    std::cout << "OK" << std::endl;


    //Je vérifie le tri de la table + je compte les uniques pour voir
    std::cout << "Tri de la table ... ";
    arcEnCiel.trier();
    uint64_t idxT = 0;
    uint64_t nbUniques = 0;
    for (uint64_t i = 0; i < context.taille_dictionnaire; i++) {
        assert(arcEnCiel.table[i].indiceT >= idxT);
        if (arcEnCiel.table[i].indiceT > idxT) {
            nbUniques++;
        }
        idxT = arcEnCiel.table[i].indiceT;
    }
    std::cout << "OK" << std::endl;

    //Je sauvegarde et reload la table, je vérifie
    std::cout << "Sauvegarde et rechargement de la table ... ";
    arcEnCiel.sauvegarder("tests/datas/actualTable64");
    ArcEnCiel arcEnCielLoad(context.M, context.T);
    arcEnCielLoad.recharger("tests/datas/actualTable64");

    //Je vérifie les valeurs dans la table
    for (uint64_t i = 0; i < context.M; i++) {
        assert(arcEnCiel.table[i].indice0 == arcEnCielLoad.table[i].indice0);
        assert(arcEnCiel.table[i].indiceT == arcEnCielLoad.table[i].indiceT);
    }
    std::cout << "OK" << std::endl;

    //Je vérifie la rechercher sur deux exemples
    uint64_t p, q;
    bool trouve = arcEnCiel.rechercher(36, p, q);
    std::cout << "rechercher de 36 : " << (trouve ? "trouvé entre les indices " : " erreur ") << p << " et " << q <<
    std::endl;
    assert(trouve && p == 16 && q == 18);
    trouve = arcEnCiel.rechercher(37, p, q);
    std::cout << "rechercher de 37 : " << (trouve ? "erreur" : "pas trouvé") << std::endl;
    assert(!trouve);

    std::cout << "Test de la table arc en ciel Réussi (valeurs uniques : " << ((double)nbUniques/(double)context.M) * 100 << "%)" << std::endl << std::endl;
}

void UnitTestTiny::testCracker(Context &context) {

    std::cout << "Test du cracker" << std::endl;
    ArcEnCiel arcEnCiel(context.M, context.T);
    arcEnCiel.recharger("tests/datas/actualTable64");

    Cracker cracker;

    //Je crack le hash de "aad", c'est c2f7ab46df3db842040a86a0897a5377 (je dois le trouver en colonne 5)
    Empreinte y;
    y.setDigest("c2f7ab46df3db842040a86a0897a5377");
    std::string clair;
    uint64_t colonne;
    std::cout << "crackage de c2f7ab46df3db842040a86a0897a5377 (aad, colonne 5) ... ";
    assert(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0));
    assert(clair == "aad");
    assert(colonne == 5);
    std::cout << "OK" << std::endl;

    //Je crack le hash de "ada", c'est f931822fed1932e33450b913054a0c3d (je dois le trouver en colonne 4)
    y.setDigest("8c8d357b5e872bbacd45197626bd5759");
    clair.clear();
    std::cout << "crackage de 8c8d357b5e872bbacd45197626bd5759 (ada, colonne 4) ... ";
    assert(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0));
    assert(clair == "ada");
    assert(colonne == 4);
    std::cout << "OK" << std::endl;

    //Je crack le hash de "ccc", c'est 9df62e693988eb4e1e1444ece0578579 (je dois le trouver en colonne 3)
    y.setDigest("9df62e693988eb4e1e1444ece0578579");
    clair.clear();
    std::cout << "crackage de 9df62e693988eb4e1e1444ece0578579 (ccc, colonne 3) ... ";
    assert(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0));
    assert(clair == "ccc");
    assert(colonne == 3);
    std::cout << "OK" << std::endl;

    //Je crack le hash de "baa", c'est 8cdcda79a8dc66aa6c711c9a000b0ac0 (je dois le trouver en colonne 2)
    y.setDigest("8cdcda79a8dc66aa6c711c9a000b0ac0");
    clair.clear();
    std::cout << "crackage de 8cdcda79a8dc66aa6c711c9a000b0ac0 (baa, colonne 2) ... ";
    assert(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0));
    assert(clair == "baa");
    assert(colonne == 2);
    std::cout << "OK" << std::endl;

    //Je crack le hash de "ddb", c'est 8494f38e85ea550d976efcec89ca67fc (je dois le trouver en colonne 1)
    y.setDigest("8494f38e85ea550d976efcec89ca67fc");
    clair.clear();
    std::cout << "crackage de 8494f38e85ea550d976efcec89ca67fc (ddb, colonne 1) ... ";
    assert(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0));
    assert(clair == "ddb");
    assert(colonne == 1);
    std::cout << "OK" << std::endl;


    //Je crack le hash d'un mot qui n'est pas dans le dictionnaire, c'est d41d8cd98f00b204e9800998ecf8427e (je ne dois pas le trouver)
    y.setDigest("d41d8cd98f00b204e9800998ecf8427e");
    clair.clear();
    std::cout << "crackage de d41d8cd98f00b204e9800998ecf8427e (pas trouvé) ... ";
    assert(!(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0)));
    std::cout << "OK" << std::endl;

    //Je crack le hash de aca, c'est 2671eb6e9150cf9b53eb39752a1fb21c (je ne dois pas le trouver)
    //On le trouverait dans la ligne qui part de indice0 = 0, mais cette ligne n'a pas été choisie lors de la construction de la table (voir actualTable)
    y.setDigest("2671eb6e9150cf9b53eb39752a1fb21c");
    clair.clear();
    std::cout << "crackage de 2671eb6e9150cf9b53eb39752a1fb21c (aca, pas trouvé) ... ";
    assert(!(cracker.cracker(y, arcEnCiel, context, clair, colonne, 0)));
    std::cout << "OK" << std::endl;

    std::cout << "Test du cracker Réussi" << std::endl << std::endl;
}
