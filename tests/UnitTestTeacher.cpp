/**
 * INFO006 - TP2 - TABLE ARC EN CIEL
 *
 * Juliana Leclaire
 * Céline de Roland
 */

#include "UnitTestTeacher.h"

/**
 * On vérifie pour chaque ligne que i2i donne le même résultat que le prof
 * Attention :
 *      Le prof numérote ses colonnes de 1 à T
 *      Nous numérotons nos colonnes de 0 à T
 *      Donc idx1Prof = idx0Nous && idxTProf = idx{T-1}Nous
 */
void UnitTestTeacher::test(Context &context, std::string nom_fichier) {

    std::cout << "Test avec les valeurs du prof" << std::endl;
    context.afficher();
    std::ifstream file("tests/datas/" + nom_fichier, std::ifstream::in);
    uint64_t indices_col_T_actual;
    if (file) {
        uint64_t idxT, idx1;
        for (uint64_t i = 0; i < context.M; i++) {
            file >> idx1 >> idxT;
            indices_col_T_actual = context.i2i(context.T - 1, idx1, 0, 0); //Voir le "Attention"
            if(this->verbose)
                std::cout << "Comparing actual = " << indices_col_T_actual << " AND expected = " << idxT << std::endl;
            else
                std::cout << ".";
            assert(indices_col_T_actual == idxT);
        }
        file.close();
    }
    else {
        std::cerr << "Impossible d'ouvrir le fichier tests/datas/" << nom_fichier << std::endl;
        assert(false);
    }

    std::cout << std::endl << "Réussi 100%" << std::endl << std::endl;
}
